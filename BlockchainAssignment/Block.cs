﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Block
    {
        public Boolean THREADING { get; set; } = true;                                 // Boolean to tell whether to use threading or not
        public List<Transaction> transactionList { get; set; }                  // List of transactions in this block
        public DateTime timeStamp { get; set; }                                 // Time of creation
        public int index { get; set; }                                          // Position of the block in the sequence of blocks
        public string hash { get; set; }                                        // The current blocks "identity"
        public string prevHash { get; set; }                                    // A reference pointer to the previous block
        public string merkleRoot { get; set; } = "0";                              // The merkle root of all transactions in the block
        public string minerAddress { get; set; }                                // Public Key (Wallet Address) of the Miner
        public double reward { get; set; } = 1;                                     // Simple fixed reward established by "Coinbase"
        public int difficulty { get; set; }                                    // An arbitrary number of 0's to proceed a hash value
        public long nonce { get; set; } = 0;                                    // Number used once for Proof-of-Work and mining

        public int nonce0 { get; set; } = 0;
        public int nonce1 { get; set; } = 1;
        public string finalHash { get; set; }
        public string finalHash0 { get; set; }
        public string finalHash1 { get; set; }

        private bool th1Fin = false, th2Fin = false;


        public Block()
        {
            this.timeStamp = DateTime.Now;
            index = 0;
            this.difficulty = 2;
            transactionList = new List<Transaction>();
            if (THREADING == true)
            {
                ThreadedMine();
                this.hash = this.finalHash;
            }
            else this.hash = this.CreateHash();
        }

        // Constructor which is not passed anything
        public Block(Block lastBlock, List<Transaction> TransactionPool, string MinerAddress, int setting, string address )
        {
            this.transactionList = new List<Transaction>();
            this.nonce = 0;
            this.timeStamp = DateTime.Now;
            this.difficulty = lastBlock.difficulty;
            this.changeDifficulty(lastBlock.timeStamp); 
            this.index = lastBlock.index + 1;
            this.prevHash = lastBlock.hash;
            this.minerAddress = MinerAddress;
            TransactionPool.Add(createRewardTransaction(TransactionPool)); // Create and append the reward transaction
            this.addFromPool(TransactionPool, setting, address );
            this.merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions

            if (THREADING == true)
            {
                this.ThreadedMine();
                this.hash = this.finalHash;
            }
            else this.hash = this.Create256Mine();
           
        }

        public override string ToString()
        {
            return ("\n\n\t\t[BLOCK START]"
                + "\nIndex: " + this.index
                + "\tTimestamp: " + this.timeStamp
                + "\nPrevious Hash: " + this.prevHash
                + "\n\t\t-- PoW --"
                + "\nDifficulty Level: " + this.difficulty
                + "\nNonce: " + this.nonce
                + "\nHash: " + this.hash + " " + this.finalHash
                + "\n\t\t-- Rewards --"
                + "\nReward: " + this.reward
                + "\nMiners Address: " + this.minerAddress
                + "\n\t\t-- " + this.transactionList.Count + " Transactions --"
                + "\nMerkle Root: " + this.merkleRoot
                + "\n" + String.Join("\n", this.transactionList)
                + "\n\t\t[BLOCK END]");
                
        }
        
        // Changes settings provided to adjust how the code runs 
        public void addFromPool(List<Transaction> TP, int mode, string address)
        {
            int LIMIT = 5;
            int idx =0 ;

            
            while (transactionList.Count < LIMIT && TP.Count > 0 ) {
                if (mode == 0 ) {// greedy
                    
                    for (int i = 0; ((i < TP.Count)); i++)
                    {
                        if (TP.ElementAt(i).Fee > TP.ElementAt(idx).Fee)
                        {
                            idx = i;
                        }
                    }
                    this.transactionList.Add(TP.ElementAt(idx));
                } 
                else if (mode == 1) {// altruistic
                    for (int i = 0; ((i < TP.Count) && (i < LIMIT)); i++)
                    {
                        this.transactionList.Add(TP.ElementAt(i));
                    }
                } 
                else if (mode == 2 ) {  //random      
                    Random random = new Random();
                    idx = random.Next(0, TP.Count);
                    this.transactionList.Add(TP.ElementAt(idx));
                }       
                else if (mode == 3) {
                    
                    //this.transactionList.Add(TP.ElementAt(0));
                    for (int i = 0; i < TP.Count && (transactionList.Count < LIMIT); i++)
                    {                       
                        if (TP.ElementAt(i).SenderAddress == address)
                        {
                            this.transactionList.Add(TP.ElementAt(i));
                        }
                        else if (TP.ElementAt(i).RecipientAddress == address)
                        {
                            this.transactionList.Add(TP.ElementAt(i));
                        }
                        else
                        {
                            
                        }
                        
                        
                    }
                    Console.WriteLine("Endless loop");
                }
                else
                { // No Valid input, choose default --> Altruistic
                    mode = 1; 
                }
                TP = TP.Except(this.transactionList).ToList();
                
            }

        }

       
        public string CreateHash()
        { 
            SHA256 hasher;
            hasher = SHA256Managed.Create();
            String input = this.index.ToString() + this.timeStamp.ToString() + this.prevHash + this.nonce + this.merkleRoot + this.reward.ToString();
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;


        }
        //Version of above method to take nonce as a parameter
        public String CreateHash(int inNonce)
        {
            SHA256 hasher;
            hasher = SHA256Managed.Create();
            String input = this.index.ToString() + this.timeStamp.ToString() + this.prevHash + inNonce + this.merkleRoot + this.reward.ToString();
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));
            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        private string Create256Mine()
        {             
            string hash =  "";          
            string diffString = new string('0', this.difficulty);
            while (hash.StartsWith(diffString)== false)
            {
                hash = this.CreateHash();
                this.nonce++;
            }
            this.nonce--;
            if (hash is null){
                throw new IndexOutOfRangeException("No hash generated"); }
            return hash;
        }

        public static string MerkleRoot(List<Transaction> transactionList) {

            // X => f(X) means given X return f(X)
            List<String> hashlist = transactionList.Select(t => t.Hash).ToList(); // Get a list of transaction hashlist for "combining"
            // Handle Blocks with...
            if (hashlist.Count == 0) // No transactions
            {
                return String.Empty;
            }
            else if (hashlist.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashlist[0], hashlist[0]);
            }
            while (hashlist.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i = 0; i < hashlist.Count; i += 2) // Step over neighbouring pair combining each
                {
                    if (i == hashlist.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashlist[i], hashlist[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashlist[i], hashlist[i + 1])); // Hash neighbours leaves
                    }
                }
                hashlist = merkleLeaves; // Update the working "layer"
            }
            return hashlist[0]; // Return the root node
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.Fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", "" , minerAddress, (this.reward + fees), 0); // Issue reward as a transaction in the new block
        }
       
        /*FOR MULTI THREADING */

        public void ThreadedMine(){
            Thread th1 = new Thread(this.Mine0);
            Thread th2 = new Thread(this.Mine1);

            th1.Start();
            th2.Start();

            while (th1.IsAlive == true || th2.IsAlive == true){Thread.Sleep(1);}

           
            if (this.finalHash1 is null) { 
                this.nonce = this.nonce0;
                this.finalHash = this.finalHash0;
            }
            else{
                this.nonce = this.nonce1;
                this.finalHash = this.finalHash1;
            }
            if (this.finalHash is null)
            {
                Console.WriteLine(this.ToString());
                throw new Exception("NULL finalhash" + 
                    " Nonce0: " + this.nonce0 + 
                    " Nonce1: "+ this.nonce1 + 
                    " Nonce: " + this.nonce +
                    " finalhash0 " + this.finalHash0 +
                    " finalhash1: " + this.finalHash1 +
                    " NewHash: " + this.CreateHash());
               
            }

        }

        public void Mine0(){
            th1Fin = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Boolean check = false;
            String newHash;
            string diffString = new string('0', this.difficulty);

            while (check == false)
            {
                newHash = CreateHash(this.nonce0);
                if (newHash.StartsWith(diffString) == true){
                    check = true;
                    this.finalHash0 = newHash;
                    Console.WriteLine("Block index: " + this.index);
                    Console.WriteLine("Thread 1 closed: Thread 1 found: " + this.finalHash0);
                    th1Fin = true;

                    Console.WriteLine(nonce0);
                    sw.Stop();
                    Console.WriteLine("Thread 1 miner:");
                    Console.WriteLine(sw.Elapsed);

                    return;
                }
                else if (th2Fin == true){
                    Console.WriteLine("Thread 1 closed: Thread 2 found: " + this.finalHash1 );
                    Thread.Sleep(1);
                    return;
                }
                else{
                    check = false;
                    this.nonce0 += 2;
                }
            }
            return;
        }

        public void Mine1()
        {
            th2Fin = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Boolean check = false;
            String newHash;
            string diffString = new string('0', this.difficulty);
            while (check == false){
                newHash = CreateHash(this.nonce1);
                if (newHash.StartsWith(diffString) == true){
                    check = true;
                    this.finalHash1 = newHash;
                    Console.WriteLine("Block index: " + this.index);
                   Console.WriteLine("Thread 2 closed: Thread 2 found: " + this.finalHash1);
                    th2Fin = true;

                    Console.WriteLine(this.nonce1);
                    sw.Stop();
                    Console.WriteLine("Thread 2 miner:");
                    Console.WriteLine(sw.Elapsed);

                    return;
                }
                else if (th1Fin == true){
                    Console.WriteLine("Thread 2 closed: Thread 1 found: " + this.finalHash0);
                    Thread.Sleep(1);
                    return;
                }
                else{
                    check = false;
                    this.nonce1 += 2;
                }
            }
            return;
        }

 

        //Function to adjust the difficulty
        public void changeDifficulty(DateTime lastTime)
        {
            //Gets the elapsed time between now and the last block mined
            DateTime startTime = DateTime.UtcNow;
            TimeSpan timeDiff = startTime - lastTime;

            //If the difference is less than 5 seconds, the difficulty is increased to attempt to increase the time
            if (timeDiff < TimeSpan.FromSeconds(5))
            {
                this.difficulty++;
                Console.WriteLine("New Difficulty:");
                Console.WriteLine(this.difficulty);
            }
            //If the difference is more than 5 seconds, the difficulty is decreased to attempt to decrease the time
            else if (timeDiff > TimeSpan.FromSeconds(5))
            {
                difficulty--;
                Console.WriteLine("New Difficulty:");
                Console.WriteLine(this.difficulty);
            }

            //Difficulty can never be higher than 7 or lower than 2
            if (this.difficulty < 2)
            {
                this.difficulty = 2;
                
            }
            else if (this.difficulty >= 7)
            {
                this.difficulty = 6;
               
            }
        }




    }

}
